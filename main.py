import pygame
import random
import utils
import message

pygame.init()

def game(config):

    clock = pygame.time.Clock()
    game = {
        "clock": clock,
        "alive": True,
        "turn": 0
    }
    speed = 2

    player = {
            "pv":3,
            "pos":[544,600]
        }

    entities = {
        "aliens":[],
        "aliensProj":[],
        "playerProj":[],
        "player": player,
        "speed":speed,
        "sounds": {
            "brrrp": pygame.mixer.Sound("./sounds/bip.wav"),
            "new_wave": pygame.mixer.Sound("./sounds/new_wave.wav"),
            "touch": pygame.mixer.Sound("./sounds/touch.wav"),
            "shoot": pygame.mixer.Sound("./sounds/shoot.wav")
        }
    }

    level1 = {"vague":[5,10,20]}
    level2 = {"vague":[10,20,40]}
    level3 = {"vague":[20,40,80]}
    # level1 = {"vague": [1,2,3]}
    # level2 = {"vague": [1,2,3]}
    # level3 = {"vague": [1,2,3]}
    levels = {"levels":[level1,level2,level3],
            "current":1,
            "last":False}

    while game["alive"]:

        game["turn"] += 1
        if game["turn"] == 3600:
            game["turn"] = 0
        
        if game["turn"]%30 == 0:
            utils.alien_fire(entities)
            if len(entities["aliens"]) == 0:
                if not levels["last"]:
                    alien_invasion_manager(entities, levels)
                    pygame.mixer.Sound.play(entities["sounds"]["new_wave"])
                else:
                    return "VICTORY"            

        if game["turn"]%2 == 0:
            landing = utils.physic_manager(config,entities)
            if landing:
                game["alive"] = False
                return "DEFEAT"

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                game["alive"] = False
                return ""
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    create_projectile(entities)
                    
        
        keys = pygame.key.get_pressed()

        if keys[pygame.K_d]:
            utils.move_player(player, 4*speed)
            if keys[pygame.K_LSHIFT]:
                utils.move_player(player,4*speed)

        if keys[pygame.K_a]:
            utils.move_player(player, -4*speed)
            if keys[pygame.K_LSHIFT]:
                utils.move_player(player, -4*speed)
            
        utils.draw_entities(config, entities)
        message.message_Manager(config, entities)
        
        pygame.display.update()

        clock.tick(60)

    return ""

def alien_invasion_manager(entities, levels):
    aliens = entities["aliens"]
    current = levels["current"]-1
    level = levels["levels"][current]
    vague = level["vague"]
    adding_aliens(entities,vague[0])
    vague.pop(0)
    if levels["current"] == 3 and len(vague) == 0:
        levels["last"] = True
    if len(vague)==0:
        levels["current"]+= 1

def create_projectile(entities):
    proj = entities["playerProj"]
    player = entities["player"]
    sound = pygame.mixer.Sound(entities["sounds"]["shoot"])

    proj.append({
        "pos": [player["pos"][0]+16, 580]
    })
    sound.play()

def create_alien():

    sens = random.randrange(2)
    speed = 4
    if sens == 0:
        direction = speed
    else:
        direction = -speed

    alien = {
        "pv":1,
        "pos": [random.randrange(1120), random.randrange(250)],
        "direction": direction
    }
    return alien

def adding_aliens(entities, number):
    i = 0
    while i < number:
        alien = create_alien()
        entities["aliens"].append(alien)
        i += 1

def menu(config):
    # Ecran de menu qui propose au joueur de faire :
    # 1 nouvelle partie
    # 2 Quitter
    params = {"texts":[
        {
        "content":"1. Nouvelle Partie",
        "pos": (128,128),
        "center": False
    },
        {
        "content":"2. Quitter le programme",
        "pos": (128,192),
        "center": False
    }
    ]}
    loop = True
    while loop:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    return "NEW"
                if event.key == pygame.K_2:
                    return "QUIT"

        utils.draw_menu(config)
        message.text_page(config, params)

        pygame.display.update()

def new_game(config):
    # Affichage de la nouvelle partie

    params = {"texts":[
        {
        "content":"NOUVELLE PARTIE",
        "pos": (1280//2, 720//2-32),
        "center": True
    },
        {
        "content":"APPUYEZ SUR UNE TOUCHE POUR COMMENCER",
        "pos": (1280//2, 720//2+32),
        "center": True
    }
    ]}

    loop = True
    while loop:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                return "NEW"
        
        utils.draw_new_game(config)
        message.text_page(config, params)

        pygame.display.update()

def victory(config):
    loop = True
    params = {"texts":[
        {
        "content":"INVASION ALIEN CONTREE",
        "pos": (1280//2, 720//2-32),
        "center": True
    },
        {
        "content":"BRAVO! UNE GRANDE VICTOIRE!",
        "pos": (1280//2, 720//2+32),
        "center": True
    }
    ]}

    while loop:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                return "QUIT"
        
        utils.draw_new_game(config)
        message.text_page(config, params)

        pygame.display.update()

def defeat(config):
    loop = True
    params = {"texts":[
        {
        "content":"QUELLE CUISANTE DEFAITE",
        "pos": (1280//2, 720//2-32),
        "center": True
    },
        {
        "content":"L'HUMANITE VA DONC ETRE REDUITE EN ESCLAVAGE",
        "pos": (1280//2, 720//2+32),
        "center": True
    }
    ]}
    while loop:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                return "QUIT"
        
        utils.draw_new_game(config)
        message.text_page(config, params)

        pygame.display.update()

def exit(config):
    loop = True
    params = {"texts":[
        {
        "content":"Merci d'avoir Joue",
        "pos": (1280//2, 720//2-32),
        "center": True
    },
        {
        "content":"A tres bientot",
        "pos": (1280//2, 720//2+32),
        "center": True
    }
    ]}    
    while loop:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                return "QUIT"
        
        utils.draw_new_game(config)
        message.text_page(config, params)

        pygame.display.update()    

def main():

    screen = pygame.display.set_mode((1280,720))
    offset = [64,40]
    font = pygame.font.Font("freesansbold.ttf", 32)

    config = {
        "screen": screen,
        "offset": offset,
        "width": 1280-offset[0]*2,
        "height": 720-offset[1]*2,
        "font" : font
    }



    choix = ""
    while choix != "QUIT":
        # Choix nouvelle partie ou quitter
        choix = menu(config)
        if choix == "NEW":
            choix = ""
            choix = new_game(config)
            if choix == "NEW":
                result = game(config)
                if result == "VICTORY":
                    victory(config)
                if result == "DEFEAT":
                    defeat(config)
    # Ecran de sortie
    exit(config)

if __name__ == "__main__":

    main()