# Projet Space Py-Invaders

Remake de space invaders avec python3 et pygame

## Fonctionnalites :

Des aliens Attaquent
Le joueur Controle un char qui peut tirer
La partie est perdues si les aliens atterrissent
La partie est perdue si le joueur meurt

## Previsions :

6 x 30 min pour coder le prototype jouable
6 x 30 min pour refactoriser et polish le jeu

10 min de preparation papier
10 min de preparation PC

## Idees :

Avoir plusieurs Niveau ou Vague
Avoir different type d'ennemy
Pouvoir utiliser d'autres armes
Ajout un effet de vent
Pouvoir placer des defense automatique
Ajout un boss Final "vaisseau mere"
Faires des graphisme

Avoir des combo d'ennemy (Exemple des petit vaisseau protecteur d'un plus gros vaisseau)
Ajouter de l'imprevisibilite dans certaines manoeuvre aliens?

Ajout de stats (nombre de tir, precision, nombre alien tues)

## Suivis :

1 ere session :
25 min, Affichage Fenetre ok, Affichage joueur ok
5 min, pause
25 min, Mouvement du Joueur ok, Debut affichage des projectiles
5 min, pause
25 min, Joueur et bordure ok, tir du char ok, Mouvement des projectile du joeur ok
Fin 1 ere session

2 eme session :
25 min, Ajout Aliens, ajout collision projectiles sur les aliens
5 min, pause
25 min, Ajout Mouvement alien, ajout colision, ajout defaite si atterrissage alien
5 min, pause
25 min, Ajout Tir Alien, et perte de partie si le char est touche
Fin 2 eme session

3 eme session : 
25 min, ajout PV, Affichage PV, affichage nombre alien, fix bug dans la collision des projectiles, victoire si 0 alien restant
5 min, pause,
25 min, Ajout de document dans utils.py, fix de bug, Ajout du menu de demarrage
Fin de session

4 eme session : 
25 min, Ajout des ecran de transition, preparation pour l'implementation des niveau
5 min, pause
25 min, Ajout des different niveau, option pour regler la vitesse du jeu
5 min, pause
25 min, Ajout sons de tir, sons de nouvelles vague, sons d'alien touche, sons de joueur touche
Fin de Session :
Reste une derniere seance pour refactoriser un peu et livrer le projet