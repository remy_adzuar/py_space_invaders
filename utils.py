import pygame
import random



# Drawing utilities

def draw_entities(config, entities):
    clear(config)
    draw_background(config)
    draw_land(config)
    draw_alien(config, entities)
    draw_player(config, entities["player"])
    draw_projectiles(config, entities)

def draw_player(config, player):

    offset = config["offset"]
    pygame.draw.rect(config["screen"], (255,255,255), 
    (offset[0]+player["pos"][0],
    offset[1]+player["pos"][1],
    32,32))

def draw_alien(config, entities):
    offset = config["offset"]
    aliens = entities["aliens"]
    for alien in aliens:
        pygame.draw.rect(config["screen"], (255,0,0),
        (offset[0]+alien["pos"][0],
        offset[1]+alien["pos"][1],
        32,32))

def draw_projectiles(config, entities):
    
    offset = config["offset"]
    
    player_proj = entities["playerProj"]
    for proj in player_proj:
        pos = proj["pos"]
        pygame.draw.rect(config["screen"], (255,255,255),
        (offset[0]+pos[0], offset[1]+pos[1],4,16))
    
    alien_proj = entities["aliensProj"]
    for proj in alien_proj:
        pos = proj["pos"]
        pygame.draw.rect(config["screen"], (255,100,0),
        (offset[0]+pos[0], offset[1]+pos[1],4,16))

def draw_background(config):
    bg_color = (0,144,255)
    pygame.draw.rect(config["screen"], bg_color, 
    (config["offset"][0],config["offset"][1],config["width"],config["height"]))

def draw_land(config):
    color = (128,255,32)
    pygame.draw.rect(config["screen"], color,
    (config["offset"][0],config["offset"][1]+544,config["width"],96))

def clear(config):
    color = (0,0,0)
    pygame.draw.rect(config["screen"], color,
    (0,0,1280,720))    

# UI drawer

def draw_menu(config):
    clear(config)
    draw_background(config)

def draw_new_game(config):
    clear(config)
    draw_background(config)

# Physics Utilities

def physic_manager(config, entities):
    # Gere la physique de chaque frame

    landing = move_aliens(entities)
    move_projectiles(config, entities)
    check_alien_projectiles(entities)
    if check_collide_player(entities):
        return True
    check_player_projectiles(config,entities)
    check_collide_alien(config,entities)
    return landing



def move_projectiles(config, entities):
    # Deplace l'ensemble des projectiles, vers le haut ou le bas
    player_proj = entities["playerProj"]
    speed = entities["speed"]

    for proj in player_proj:
        pos = proj["pos"]
        pos[1]-= 8*speed
    
    alien_proj = entities["aliensProj"]
    for proj in alien_proj:
        pos = proj["pos"]
        pos[1] += 4*speed

def check_player_projectiles(config, entities):
    # Verifie que le projectile ne sorte pas de l'ecran par le haut
    offset = config["offset"]

    player_proj = entities["playerProj"]

    i = len(player_proj)-1
    while i >= 0:
        pos = player_proj[i]["pos"]
        if pos[1] < 0:
            player_proj.pop(i)
        i -= 1

def check_collide_alien(config, entities):
    # Verifie les rencontre en les projectiles du joueur et les aliens
    aliens = entities["aliens"]
    projectiles = entities["playerProj"]
    sound = pygame.mixer.Sound(entities["sounds"]["brrrp"])

    i = len(aliens)-1
    while i >= 0:

        alien = aliens[i]
        alien_pos = alien["pos"]

        j = len(projectiles)-1
        # Bug a fouiller
        while j >= 0:
            proj = projectiles[j]
            proj_pos = proj["pos"]
            if proj_pos[0]-4 >= alien_pos[0] and proj_pos[0] <= alien_pos[0]+32:
                if proj_pos[1] >= alien_pos[1] and proj_pos[1] < alien_pos[1]+32:
                    aliens.pop(i)
                    projectiles.pop(j)
                    sound.play()
                    sound.set_volume(0.1)
            j -= 1

        i -= 1

def check_player_pos(player):
    # Verifie que le joueur ne depasse pas des bordure
    pos = player["pos"]
    if pos[0]<=0:
        pos[0] = 0
    if pos[0] >= 1120: #1152-32, 1152 la taille du terrain, 32 etant la taille du char
        pos[0] = 1120

def move_player(player, direction):
    # Deplace le joueur puis verifie sa position par rapport aux bordure
    player["pos"][0] += direction
    check_player_pos(player)

def move_aliens(entities):
    # Parcours la liste des aliens pour les faire se deplacer
    aliens = entities["aliens"]
    speed = entities["speed"]

    for alien in aliens:
        landing = move_alien(alien, speed)
        if landing:
            return True
    return False

def move_alien(alien, speed):
    # Fonction qui deplacement les alien de gauche a droite, droite a gauche et vers le bas
    pos = alien["pos"]
    direction = alien["direction"]
    
    pos[0] += direction*speed
    if pos[0] <= 0:
        pos[0] = 0
        alien["direction"] = alien["direction"]*-1
        pos[1] += 32
    if pos[0] >= 1120:
        pos[0] = 1120
        alien["direction"] = alien["direction"]*-1
        pos[1] += 32
    
    if pos[1] >= 512: # 544-32, 544 etant la frontiere entre le ciel et lespace, 32 la taille de l'alien
        return True
    return False

def create_alien_projectile(entities, pos):
    # Creer un projectile alien dans la liste idoine
    proj_pos = [pos[0]+16,pos[1]+32]
    entities["aliensProj"].append({"pos":[proj_pos[0],proj_pos[1]]})

def alien_fire(entities):
    # Declenche le tire d'un alien au hasard sur le niveau
    aliens = entities["aliens"]
    if aliens != []:

        fireRate = len(aliens)//5+1
        cpt = 0
        while cpt < fireRate:
            i = random.randrange(len(aliens))
            print(i)
            if i>=0:
                pos = aliens[i]["pos"]
                create_alien_projectile(entities, pos)
            cpt += 1

def check_alien_projectiles(entities):
    # Check si le projectiles s'est ecrase au sol
    projectiles = entities["aliensProj"]
    i = len(projectiles)-1
    while i>= 0:
        proj = projectiles[i]
        pos = proj["pos"]
        if pos[1]+16 >= 632:
            projectiles.pop(i)
        i -= 1


def check_collide_player(entities):
    # Check la collision entre les projectiles alien et le char du joueur
    projectiles = entities["aliensProj"]
    player = entities["player"]
    plyr_pos = player["pos"]
    sound = entities["sounds"]["touch"]

    i = len(projectiles)-1
    while i>= 0:
        proj = projectiles[i]
        pos = proj["pos"]
        if pos[0]-4 >= plyr_pos[0] and pos[0] <= plyr_pos[0]+32:
            if pos[1]+16 >= plyr_pos[1] and pos[1]+16 <= plyr_pos[1]+32:
                
                projectiles.pop(i)
                pygame.mixer.Sound.play(sound)
                return player_health(entities["player"], -1)

        i -= 1
    return False


def player_health(player, damage):
    #Modifie la sante du joueur
    player["pv"] += damage
    if player["pv"] <= 0:
        return True
    return False
