import pygame

WHITE = (255,255,255)
GREY = (100,100,100)

def message_Manager(config,entities):
    alien_restant(config, entities)
    player_pv(config, entities)

def alien_restant(config, entities):
    font = config["font"]
    aliens = len(entities["aliens"])
    text = font.render("Aliens:"+str(aliens), True, WHITE, GREY)
    config["screen"].blit(text, (640,0))

def player_pv(config, entities):
    font = config["font"]
    pv = entities["player"]["pv"]
    text = font.render("PV:"+str(pv)+"/3", True, WHITE, GREY)
    config["screen"].blit(text, (960,0))

def player_fire(config, entities):
    pass

def alien_fire(config, entities):
    pass


# Message pour l'UI

def text_page(config, params):

    font = config["font"]
    texts = params["texts"]
    for text in texts:
        temp = font.render(text["content"], True, WHITE, GREY)
        if (text["center"]):
            rect = temp.get_rect()
            rect.center = text["pos"]
            config["screen"].blit(temp, rect)
        else:
            config["screen"].blit(temp, text["pos"])